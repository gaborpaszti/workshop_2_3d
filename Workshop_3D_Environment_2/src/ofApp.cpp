#include "ofApp.h"
#include<iostream>

//--------------------------------------------------------------
void ofApp::setup() {

	ofSetFrameRate(30);
	ofBackground(0);

	ofDisableAlphaBlending();
	ofEnableDepthTest();


	cam.setPosition(0, 0, 0); //it was (0, 0, 20);
	cam.lookAt(ofVec3f(0, 0, 0));  ///changing this parameters will set the camera defult at certain angle 


	ofDisableArbTex();                             ///Texture Loading
	ofLoadImage(mTex, "EyeX.jpg");

	light.enable();                                ///Light projection on the object
	light.setPosition(ofVec3f(100, 100, 200));
	light.lookAt(ofVec3f(0, 90, 0));


	cam.tiltDeg(80);  /// this is to set the camera starting position in an agle for better observation of the 3d primitive
	cam.dolly(60);



	///verteses
	for (int i = -numCols / 2; i < numCols / 2; i++) {
		for (int j = -numRows / 2; j < numRows / 2; j++) {

			mesh.addVertex(ofPoint(i*spacing, j*spacing, 0)); // make a new vertex
			mesh.addColor(ofFloatColor(255, 0, 0));  // add a color at that vertex

		}
	}


	/// Set up triangles' indices
	/// only loop to -1 so they don't connect back to the beginning of the row
	for (int x = 0; x < numCols - 1; x++) {
		for (int y = 0; y < numRows - 1; y++) {
			int topLeft = x + numCols * y;
			int bottomLeft = x + 1 + numCols * y;
			int topRight = x + numCols * (y + 1);
			int bottomRight = x + 1 + numCols * (y + 1);

			mesh.addTriangle(topLeft, bottomLeft, topRight);
			mesh.addTriangle(bottomLeft, topRight, bottomRight);

		}
	}


}

//--------------------------------------------------------------
void ofApp::update() {

	for (int i = 0; i < mesh.getVertices().size(); i++) {
		float x = mesh.getVertex(i).x;
		float y = mesh.getVertex(i).y;

		mesh.setVertex(i, ofVec3f(x, y, getZNoiseValue(x, y)));
	}

}

//--------------------------------------------------------------
void ofApp::draw() {

	cam.begin();
	box.set(10);
	//box.setWidth(10); //// could be doing manually if needed
	//box.setHeight(10);
	box.setPosition(0, 0, 0);
	mTex.bind();
	box.draw();
	mTex.unbind();

	box.set(5);
	box.setPosition(15, -15, getZNoiseValue(15, 15));;
	mTex.bind();
	box.draw();
	mTex.unbind();
	 
	box.set(3);
	box.setPosition(-10, -10, getZNoiseValue(-10, -10));
	mTex.bind();
	box.draw();
	mTex.unbind();

	box.set(2);
	box.setPosition(-15, -15, getZNoiseValue(-15, -15));
	mTex.bind();
	box.draw();
	mTex.unbind();

	box.set(2);
	box.setPosition(8, 8, getZNoiseValue(15, 15));
	mTex.bind();
	box.draw();
	mTex.unbind();

	box.set(7);
	box.setPosition(-15, 15, getZNoiseValue(-5, -5));
	mTex.bind();
	box.draw();
	mTex.unbind();

	mTex.bind();
	box.draw();
	mTex.unbind();


	mesh.drawVertices();    ////check
	ofSetColor(255, 255, 255);
	mesh.drawWireframe();
	cam.end();

}



//--------------------------------------------------------------
void ofApp::keyPressed(int key) {     ////camera controll logic

	if (key == 's') {
		cam.dolly(10);
	}
	if (key == 'w') {
		cam.dolly(-10);
	}
	if (key == 'a') {
		cam.panDeg(-10);
	}
	if (key == 'd') {
		cam.panDeg(10);
	}
	if (key == 'q') {
		cam.tiltDeg(-10);
	}
	if (key == 'e') {
		cam.tiltDeg(10);
	}
}



///perlin noise on the mesh function
float ofApp::getZNoiseValue(int x, int y) {  

	float time = ofGetElapsedTimef();   /// making a variable time connecting it to elapsed time
	
	float n1_freq = time * ofRandom(-0.1, 0.1);
	float n2_freq = time * ofRandom(-0.1, 0.1);
	float n1_amp = time / 2 * ofRandom(-0.1, 0.1);
	float n2_amp = time / 2 * ofRandom(-0.1, 0.1);

	n1 = ofNoise(x * n1_freq, y * n1_freq) * n1_amp;
	n2 = ofNoise(x * n2_freq, y * n2_freq) * n2_amp;

	/////cout << n1 + n2 << endl;
	return n1 + n2;

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
